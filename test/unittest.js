/* ----------------------------------------------------------------------------
    pusher.flatdb
    MIT License, Copyright (c) 2013 Pusher, Inc.
   -------------------------------------------------------------------------- */
/*
    Permission is hereby granted, free of charge, to any person obtaining a 
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation 
    the rights to use, copy, modify, merge, publish, distribute, sublicense, 
    and/or sell copies of the Software, and to permit persons to whom the 
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in 
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
   ----------------------------------------------------------------------------
*/

var nodeenv     = require("pusher.nodeenv"),
    flatdb      = require("../lib/flatdb.js"),
    fs          = require("fs"),
    path        = require("path"),
    _           = require("underscore");
    
//
// Start with a set of general tests
//
var suite = 
{
    "Detail" :
    {
        "nestedKey" : function (test)
        {
            var set = flatdb.detail.setNestedKey;
            
            var h = {};
            set(h, "param.a.b", 7);
            test.equal(h.param.a.b, 7);
            test.notEqual(h.param.a.b, 8);
            test.strictEqual(h.param.a.b, 7);
            
            set(h, "param.delta", "alpha");
            test.strictEqual(h.param.delta, "alpha");
            
            set(h, "param.beta", { x: "alpha", y : 8 });
            test.strictEqual(h.param.beta.x, "alpha");
            test.strictEqual(h.param.beta.y, 8);
            test.strictEqual(h.param.delta, "alpha");
            test.strictEqual(h.param.a.b, 7);
        
            test.done();
        }
    },

    "Error Conditions" : 
    {
    },
    
    "Tree-001" :
    {
        setUp : function(done)
        {
            var toErase = [
                "./data/tree-001/portfolio/client3.motto.txt",
                "./data/tree-001/portfolio/client3.unneeded.txt"
            ];
        
            fs.writeFileSync("./data/tree-001/portfolio/client2.notes.txt", "Notes on regarding client 2.\n");
            
            _.each(toErase, function (filename) 
            {
                if (fs.existsSync(filename))
                    fs.unlinkSync(filename);
            });
            
            this.db = flatdb.open("./data/tree-001", function(err, json) {
                done();
            });
        },
        tearDown : function(done)
        {
            this.db.close();
            done();
        },
        
        "Base Content" : function (test)
        {            
            test.notEqual(this.db, null);
            test.notEqual(this.db.data(), null);
            
            var obj = this.db.data();
            
            var table = [
                [ obj.page.title,                   "My Homepage" ],
                [ obj.page.version,                 "0.1.0"       ],
                [ obj.portfolio.client1.name,       "Client 1"    ],
                [ obj.portfolio.client2.name,       "Client 2"    ],
                [ obj.portfolio.client3.name,       "Client 3"    ],
                [ obj.portfolio.client2.notes,      "Notes on regarding client 2.\n" ],
            ];
            _.each(table, function (row) {
                test.strictEqual(row[0], row[1]);
            });
            
            test.strictEqual(obj.portfolio.client1.notes, undefined);
            
            test.done();
        },
        
        "Affected Attributes" : function (test)
        {
            var db = this.db;            
            var akeys = function (rel) {
                return db.assetKeys(path.resolve(path.join(db.basePath, rel)));
            };
            
            var r = akeys("portfolio/client2.notes.txt");
            test.deepEqual( _.keys(r), [ "portfolio.client2.notes" ]);
            test.deepEqual( _.values(r), [ [ ] ]);
            
            var r = akeys("page.json");
            test.deepEqual( _.keys(r), [ "page" ]);
            test.deepEqual( _.values(r), [ [ "title", "version" ] ]);
            
            test.done();
        },
        
        "Modify File" : function (test)
        {
            var db = this.db;
            var obj = this.db.data();
            var file       = "./data/tree-001/portfolio/client2.notes.txt";
            var oldContent = "Notes on regarding client 2.\n";
            var newContent = "Modified value";
            
            // Check our basis is correct
            test.strictEqual(obj.portfolio.client2.notes, oldContent);
                        
            db.once("modify", function() {
                test.strictEqual(obj.portfolio.client2.notes, newContent);
                db.once("modify", function() {
                    test.done();
                });
                fs.writeFileSync(file, oldContent);
            });
            fs.writeFileSync(file, newContent);
        },
        
        "Add File" : function (test)
        {
            var db      = this.db;
            var obj     = this.db.data();
            var file    = "./data/tree-001/portfolio/client3.motto.txt";
            var content = "We do it right.";
            
            test.strictEqual(obj.portfolio.client3.motto, undefined);
            
            db.once("add", function (filename) {
                var expect = path.basename(file);
                var actual = path.basename(filename);
                test.equal(expect, actual);
                
                test.strictEqual(obj.portfolio.client3.motto, content);
                test.done();
            });
            fs.writeFileSync(file, content);            
        },
        
        "Erase File" : function (test)
        {
            var db      = this.db;
            var obj     = this.db.data();
            var file    = "./data/tree-001/portfolio/client3.unneeded.txt";
            var content = "Excess content.";
            
            var stage = new events.EventEmitter;
            
            stage.on("writeFile", function() 
            {
                test.strictEqual(obj.portfolio.client3.unneeded, undefined);
                db.once("add", function(filename) {
                    var expect = path.basename(file);
                    var actual = path.basename(filename);
                    test.equal(expect, actual);
                    test.strictEqual(obj.portfolio.client3.unneeded, content);
                    stage.emit("eraseFile");
                });
                fs.writeFileSync(file, content); 
            });

            stage.on("eraseFile", function() 
            {
                test.strictEqual(obj.portfolio.client3.unneeded, content);
                db.once("erase", function(filename) {
                    var expect = path.basename(file);
                    var actual = path.basename(filename);
                    test.equal(expect, actual);
                    test.strictEqual(obj.portfolio.client3.unneeded, undefined);
                    stage.emit("finish");
                });
                fs.unlinkSync(file);
            });

            stage.on("finish", function() {
                test.done();
            });
            
            stage.emit("writeFile");        
        }
    }
};


exports = _.extend(exports, nodeenv.nodeunit.suite, suite);
