/* ----------------------------------------------------------------------------
    pusher.flatdb
    MIT License, Copyright (c) 2013 Pusher, Inc.
   -------------------------------------------------------------------------- */
/*
    Permission is hereby granted, free of charge, to any person obtaining a 
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation 
    the rights to use, copy, modify, merge, publish, distribute, sublicense, 
    and/or sell copies of the Software, and to permit persons to whom the 
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in 
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
   ----------------------------------------------------------------------------
*/
//-----------------------------------------------------------------------------------//
// ROADMAP
//-----------------------------------------------------------------------------------//
/*
    v0.0.5    
    - Track the attributes that an asset (i.e. file) contributes to the data object
      so that if that object is erased or modified, those attributes can be removed.
    - Treat binaries as filenames
    - parseOnce() function (i.e. read everything, no file watching)
    
    v0.1.0    
    - Plug-ins for how to parse based on file type
    - Track the addition / removal of directories as well as files
   
    v1.0.0
    - Plug-in for markdown to HTML conversion
    - Explicit rescan() function that optionally re-occurs every N milliseconds
      (Might be waaay more effective than the unstable watch() implementation!)
 */

//-----------------------------------------------------------------------------------//
// Dependencies
//-----------------------------------------------------------------------------------//

var fs      = require("fs"),
    path    = require("path"),
    util    = require("util"),
    events  = require("events"),
    _       = require("underscore");

var lib = {};    
    
//-----------------------------------------------------------------------------------//
// Utility and helper methods
//-----------------------------------------------------------------------------------//        

var setNestedKey = function (obj, nestedKey, value) 
{
    // Split the array into parts
    var a = nestedKey.split('.');
    
    for (var i = 0; i < a.length - 1; ++i) {
        var key = a[i];
        obj[key] = obj[key] || {};
        obj = obj[key];
    }
    var last = a[a.length - 1];
    obj[last] = obj[last] || {};
    
    var type = typeof value;
    if (type === "object")
        _.extend(obj[last], value);
    else
        obj[last] = value;
};

var eraseNestedKeys = function (obj, nestedKey, keys) 
{
    var kk = obj;
    
    // Split the array into parts
    var a = nestedKey.split('.');
    
    var lastKey = a[a.length - 1];
    for (var i = 0; i < a.length - 1; ++i) {
        var key = a[i];
        obj = obj[key];
    }   
    
    if (keys)
    {        
        var o = obj[lastKey];
        _.each(keys, function(k) {
            delete o[k];
        });
        if (_.keys(o).length > 0)
            keys = null
    }
    
    if (!keys)
    {
        delete obj[lastKey];
    }
};

var assetTransform = function (baseDir, fullpath, includeExt)
{
    var full = fullpath;
    var rel = path.relative(baseDir, full);
    var ext = path.extname(full);
    var keybase = path.join(path.dirname(rel), path.basename(rel, ext));
    
    var obj = 
    {
        path : path.resolve(full),
        key  : keybase.replace(new RegExp("\\" + path.sep, "g"), ".")
    };
    if (includeExt)
        obj.ext = ext;
    return obj;
};

/*
    Expose these primarily for unit testing purposes.
    
    Hopefully the "detail" namespace may hint they are not designed for general
    use -- or at least are considered unsupported.
 */
lib.detail = 
{
    setNestedKey    : setNestedKey,
    eraseNestedKeys : eraseNestedKeys
};
    
//-----------------------------------------------------------------------------------//
// Implementation
//-----------------------------------------------------------------------------------//       

var FlatDb = function (basePath)
{
    // Call parent constructor
    events.EventEmitter.call(this);

    this.basePath = path.normalize(basePath);
    this.assets = {};
    this.watchList = {};
    this.rootObject = {};
    this.lastRefresh = {};
};
util.inherits(FlatDb, events.EventEmitter);

(function() 
{
    var methods = {};
    
    methods.data = function()
    {
        return this.rootObject;
    };
    
    methods.assetKeys = function (fullpath)
    {
        console.log(fullpath);
        var attrs = this.assets[fullpath];
        if (attrs)
        {
            var obj = {};
            obj[attrs.base] = attrs.keys || [];
            return obj;
        }
    };
    
    /*!
        @todo Break this out into a pluggable architecture
    */
    methods._addAsset = function (entry, callback)
    {
        var self = this;
        callback = callback || function() {};
    
        fs.readFile(entry.path, "utf8", function (err, text) 
        {
            if (err)
                callback(err, null);
            else
            {
                var value = text;
                if (entry.ext == ".json") 
                {
                     try 
                     {
                        // Treat blank files as empty objects
                        if (text.length > 0)
                            value = JSON.parse(text);
                        else
                            value = {};
                     } 
                     catch (e)
                     {
                        console.log("Error parsing JSON in: " + entry.path);
                        throw e;
                     }
                }

                setNestedKey(self.rootObject, entry.key, value);
                
                //
                // Cache the keys this asset affects so that if it is 
                // removed, we know which attributes to remove in turn.
                // Note that assets with overlapping or duplicate keys
                // are not supported due to the complexity it would cause
                // here...
                //
                var keys = null;
                if (typeof value === "object")
                    keys = _.keys(value);
                
                self.assets[entry.path] =  {
                    base : entry.key,
                    keys : keys
                };            
                
                callback(null, self.rootObject);
            }
        });

    };
    
    methods._removeAsset = function (entry)
    {
        var self = this;
        var attrs = self.assets[entry.path].keys;        
        delete self.assets[entry.path];
        
        eraseNestedKeys(self.rootObject, entry.key, attrs);
    };
    
    
    /*!@todo What about removed directories?
    
        Need a mapping of all files to keys it affects.
        Pretty heavy-weight mapping...
     */
     
    methods.refreshWatchList = (function() 
    {
        //
        // Don't trust the "event" argument - it seems to be unreliable.
        // Instead base it off what we already know -- plus what we 
        // seem to be able to trust about the event.  Note: this is delicate,
        // platform dependent code.
        //
        var listenWorker = function (db, entry, unreliableEvent, filename)
        {    
            var fullpath;
            if (!entry.ext) 
            {                
                if (filename)
                {
                    fullpath = path.join(entry.path, filename);
                    
                    // Use the watchList to weed out any calls on directories.
                    // Trading clarity for the efficiency of avoided file i/o 
                    // here.
                    if (db.watchList[fullpath])
                        fullpath = null;
                }
                else
                {
                    //
                    // If it didn't come with a filename, that means we
                    // need to *find* the newly added file
                    //
                    // Laziness - synchronous version until this is working
                    var files = fs.readdirSync(entry.path);
                    _.each(files, function (file) {
                        file = path.join(entry.path, file);
                        if (!db.assets[file])
                            fullpath = file;
                    });
                    
                    //
                    // This is ridiculous, but it's a workable stub: we
                    // didn't find the file, so search EVERY file to see
                    // which one is now gone.
                    //
                    _.each(_.keys(db.assets), function (file) {
                        if (!fs.existsSync(file))
                            fullpath = file;
                    });
                }
            }       

            if (fullpath) fs.exists(fullpath, function(exists) 
            {
                var asset = assetTransform(db.basePath, fullpath, true);

                var type = "";
                if (!exists) 
                    type = "erase";
                else if (db.assets[fullpath])
                    type = "modify";
                else 
                    type = "add";
                    
                //
                // Due to unreliable implementations, double events are sometimes
                // sent.  Filter these out by tracking the same event happening
                // quickly in succession.
                //
                var now       = new Date().getTime();
                var lastEvent = db.lastRefresh[fullpath] || {};                    
                if (lastEvent.type != type || true) //now - 30 > lastEvent.timestamp)
                {
                    db.lastRefresh[fullpath] = { type : type, timestamp : now };
                                                        
                    var emit = function (err, json) {
                        db.emit("change", fullpath);
                        db.emit(type, fullpath);
                    };
                    
                    switch (type)
                    {
                    case "add":
                        db._addAsset(asset, emit);
                        break;
                    case "modify":
                        db._removeAsset(asset);
                        db._addAsset(asset, emit);
                        break;
                    case "erase":
                        db._removeAsset(asset, emit);                        
                        emit();
                        break;
                    }
                }
            });
        };
    
        /*
            Awkward workaround for a double-triggered event on Windows
            currently.  Track the last invokation for that filename
            and throttle any closely followed triggers.
         */
        var addListener = function (db, entry)
        {
            return function (event, filename)
            {
                return listenWorker(db, entry, event, filename);
            };
        };
    
        return function (results, callback)
        {
            var self = this;
            _.each(results.directories, function (entry)
            {
                var dir = entry.path;
                if (!self.watchList[dir]) 
                {
                    self.watchList[dir] = fs.watch(dir, addListener(self, entry));                
                }
            });
            callback(null);        
        };
    })();
    
    methods.close = function()
    {
        var data = this.rootObject;
        
        _.each(this.watchList, function (watcher) {
            watcher.close();
        });
        this.lastRefresh = {};
        this.rootObject = {};
        
        return data;
    };
    
    _.extend(FlatDb.prototype, methods);
})();



/*!
    ## scanDirectory (baseDir, callback(err, files) )

    Recursivesly scan the directory, getting the list of 
    files and directories therein (including the base
    directory).
 */
lib.scanDirectory = (function()
{
    var worker = function (dir, results, done)
    {
        results.directories.push(path.resolve(dir));
        
        fs.readdir(dir, function (err, files) 
        {        
            var subdirs = [];
            
            var next = _.after(files.length, function() 
            {                
                var dirdone = _.after(subdirs.length, function() 
                {
                    done(null, results);
                });                
                _.each(subdirs, function(subdir) 
                {
                    worker(subdir, results, dirdone);
                });
            });
            
            _.each(files, function (file) 
            {
                var filepath = path.join(dir, file);
                fs.stat(filepath, function (err, stats) 
                {
                    if (stats.isFile())
                        results.files.push(filepath);
                    else if (stats.isDirectory())
                        subdirs.push(filepath);    
                    next();
                });
            });
        });
    };
    
    return function (dir, callback)
    {
        var results = 
        {
            files : [],
            directories  : []
        };
        worker(dir, results, callback);
    };
    
})();

/*!
    Recursively scan the directory and return each found file as
    a associative array "key" along with the full path and 
    extension of the file.
 */
lib.scanDirectoryDbEntries = function(baseDir, callback) 
{                   
    lib.scanDirectory(path.normalize(baseDir), function(err, results) 
    {
        // Transform the results
        callback(null, {
            directories : _.map(results.directories, function (dir) {
                return assetTransform(baseDir, dir, false);
            }),    
            files : _.map(results.files, function (file) {
                return assetTransform(baseDir, file, true);
            })
        });
    });
};

lib.readJSONData = function (db, results, callback)
{
    var json = {};
    var done = _.after(results.files.length, function () {
        callback(null, json);
    });
    
    _.each(results.files, function (file)
    {       
        db._addAsset(file, done);
    });
};

lib.processDirectory = function (db, callback)
{
    lib.scanDirectoryDbEntries(db.basePath, function (err, results) 
    {
        db.rootObject = {};
        var next = _.after(2, function() {
            callback(err, db.rootObject);
        });
    
        lib.readJSONData(db, results, function (err, json) {                    
            next();
        });
        db.refreshWatchList(results, function (err) {
            next();
        });
    });
};

lib.watch = lib.open = function (baseDir, callback)
{

    var db = new FlatDb(baseDir);
    lib.processDirectory(db, callback);
    return db;
};

module.exports = lib;
