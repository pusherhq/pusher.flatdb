#README

pusher.flatdb is a node.js module for building a large JSON object from a 
flat-file / folder structure.  It's intended for sites or applications with 
a relatively small, static set or subset of content where easy of updating 
is a priority over raw performance.

## Installation

``npm install pusher.flatdb``

## How it works

``pusher.flatdb`` takes a base directory name and walks the files and folders in that directory.  For every file found, the file basename (i.e. without the extension) is used as the attribute key and the file contents becomes the value.  If the file is a JSON file, the value is parsed as JSON -- otherwise, a string is generated equal to the file contents.  In the case of binary files, the filename is stored rather than the file contents.

### Nested fields

Dots in the filenames before the extension

Example:

    basedir\
        myobject.json
        myobject.fieldName.html


## FAQ

### What about creating indexed arrays from files?

There's no "native" support.  A file-based structure requires unique names for each object anyway -- so, it's recommended that a simple call using something like ``underscore.js``'s _.values() be made on the associative array to treat it like an indexed array.

If you need a specific order, you'll need to sort by the keys.

Of course, any values in a JSON file itself can use indexed arrays directly.

### What about duplicating values?

Try to avoid it...much of the code is based on the assumption
of no duplicate (or overlapping) keys in the overall structure.

As such, pusher.flatdb makes no guarentees about a particular 
field value if it is specified differently in multiple places.  
